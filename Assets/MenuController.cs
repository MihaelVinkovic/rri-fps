﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    public GameObject[] buttoni;
    public GameObject[] kakoIgrati;
    

	// Use this for initialization
	void Start () {
        for(int i = 0; i< kakoIgrati.Length; i++)
        {
            kakoIgrati[i].SetActive(false);
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void KakoIgrati()
    {
        for(int i = 0; i<buttoni.Length; i++)
        {
            buttoni[i].SetActive(false);
        }

        for (int j = 0; j < kakoIgrati.Length; j++)
        {
            kakoIgrati[j].SetActive(true);
        }
    }


    public void Povratak()
    {
        for (int i = 0; i < buttoni.Length; i++)
        {
            buttoni[i].SetActive(true);
        }

        for (int j = 0; j < kakoIgrati.Length; j++)
        {
            kakoIgrati[j].SetActive(false);
        }
    }
}
