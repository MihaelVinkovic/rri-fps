﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public AudioClip pucanje;
    public AudioClip pozadina;

    public AudioSource pucanjeIzvor;
    public AudioSource pozadinaIzvor;

    public bool svira = false;
    

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
        pozadinaIzvor.clip = pozadina;
        pozadinaIzvor.Play();
        pozadinaIzvor.loop = true;
        pucanjeIzvor.clip = pucanje;
        pucanjeIzvor.loop = true;
    }
	
    public void pucaj()
    {
        //pucanjeIzvor.clip = pucanje;
        if (!svira)
        {
            pucanjeIzvor.Play();            
            svira = true;
        }
    }
    
    public void pucanjeStoj()
    {
        pucanjeIzvor.Stop();
        svira = false;
    }


	// Update is called once per frame
	void Update () {
        
	}
}
