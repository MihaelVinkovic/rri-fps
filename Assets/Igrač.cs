﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Igrač : MonoBehaviour {

    public GameObject puška;
    public GameObject ograda;
    public GameObject faza2Start;

    public float municija = 0;
    public float zivot = 100;

    private CanvasKontroler cK;
    private DrugaFaza df;

    

	// Use this for initialization
	void Start () {
        cK = FindObjectOfType<CanvasKontroler>();

        faza2Start.SetActive(false);
        
	}
	
	// Update is called once per frame
	void Update () {
        cK.PostaviTekstZaMuniciju(municija.ToString());
        cK.KoličinaŽivota(zivot);


        if(zivot <= 0)
        {
            // učitaj scenu gubitnika
            SceneManager.LoadScene(4);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Oruzje")
        {
            puška.SetActive(true);
            Destroy(other.gameObject);
            cK.PostaviTekstZaMisiju("Pronađi metke!");
        }

        if(other.gameObject.tag == "Municija")
        {
            if (puška.activeSelf)
            {
                municija = 1000;          
                Destroy(other.gameObject);
                cK.PostaviTekstZaMisiju("Pronađi ulaz u dvorac!");
                ograda.SetActive(false);
            }
        }

        if(other.gameObject.tag == "Win")
        {
            //Učitaj scenu pobjenika
            SceneManager.LoadScene(3);
        }

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Neprijatelj")
        {
            zivot -= 30f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Faza2")
        {
            
            df = FindObjectOfType<DrugaFaza>();
            df.Igra();
            cK.PostaviTekstZaMisiju("Uništi sve zombije !\nNivo " + df.nivo.ToString() + "/5");
            faza2Start.SetActive(true);

        }
    }


}