﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puška : MonoBehaviour {

    public float šteta = 10f;
    public float domet = 100f;

    public Camera fpsKamera;
    public GameObject pucanje;

    private Igrač igrac;

    private MusicManager mm;
    private void Start()
    {        
        pucanje.SetActive(false);
        igrac = FindObjectOfType<Igrač>();
        mm = FindObjectOfType<MusicManager>();
    }


    // Update is called once per frame
    void Update () {
        if (Input.GetButton("Fire1")){
            Pucaj();
        }else if (!Input.GetButton("Fire1"))
        {
            pucanje.SetActive(false);
            mm.pucanjeStoj();
        }
    }

    private void Pucaj()
    {
        if (igrac.municija > 0)
        {            
            mm.pucaj();
           
            RaycastHit hit;
            if (Physics.Raycast(fpsKamera.transform.position, fpsKamera.transform.forward, out hit, domet))
            {
                Debug.Log(hit.transform.name);
                if (igrac.municija > 0)
                {
                    igrac.municija -= 1;
                    pucanje.SetActive(true);
                }
                if (hit.transform.tag == "Neprijatelj")
                {
                    //ne destroy nego oduzmi zivot
                    GameObject neprijatelj = hit.transform.gameObject;
                    NeprijateljKontroler nK = neprijatelj.GetComponent<NeprijateljKontroler>();
                    nK.zivot -= 15f;
                    //Destroy(hit.transform.gameObject);
                }
            }
        }
    }
}
