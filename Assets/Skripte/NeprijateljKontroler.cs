﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class NeprijateljKontroler : MonoBehaviour {

    [HideInInspector]
    public float zivot = 150;
    private Igrač igrac;


    // Use this for initialization
    void Start () {
        igrac = FindObjectOfType<Igrač>();
        GetComponent<AICharacterControl>().target = igrac.transform;
    }
	
	// Update is called once per frame
	void Update () {
		if(zivot <= 0)
        {
            Destroy(gameObject);
        }
	}
}
