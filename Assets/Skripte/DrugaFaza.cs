﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;


public class DrugaFaza : MonoBehaviour {

    public List<GameObject> neprijateljiPozicije = new List<GameObject>();
    public List<GameObject> naprijatelji = new List<GameObject>();

    public GameObject stube1;
    public GameObject stube2;

    [HideInInspector]
    public int nivo = 0;
    [HideInInspector]
    public bool faza2 = false;
    private bool inGame = false;



    private CanvasKontroler ck;
    private 
    

	// Use this for initialization
	void Start () {
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("Pozicije"))
        {
            neprijateljiPozicije.Add(g);
        }      

        stube1.SetActive(false);
        stube2.SetActive(false);


        ck = FindObjectOfType<CanvasKontroler>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (faza2)
        {
            ck.PostaviTekstZaMisiju("Uništi sve zombije !\nNivo " + nivo.ToString() + "/5");
            if (!GameObject.FindGameObjectWithTag("Neprijatelj"))
            {
                  inGame = false;
                  Igra();
                
            }
            else if(!GameObject.FindGameObjectWithTag("Neprijatelj") && nivo == 5)
            {
                ck.PostaviTekstZaMisiju("Pronađi izlaz !");
                stube1.SetActive(true);
                stube2.SetActive(true);

            }
            
        }
        

    }

   


    public void Igra()
    {

        if(nivo < 5 && !inGame)
        {
            for(int i = 0; i <= neprijateljiPozicije.Count-1; i++)
            {
                Instantiate(naprijatelji[Random.Range(0, 2)], neprijateljiPozicije[i].transform.position, Quaternion.identity);
            }
            nivo++;
            inGame = true;
            faza2 = true;
            Debug.Log("Nivo: " +nivo);
            
        }else if(nivo >= 5)
        {
            ck.PostaviTekstZaMisiju("Pronađi izlaz !");
            stube1.SetActive(true);
            stube2.SetActive(true);
        }
       
       
    }
}