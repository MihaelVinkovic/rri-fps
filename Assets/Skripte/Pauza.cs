﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Pauza : MonoBehaviour {

    public GameObject game1;
    public GameObject game2;
    public GameObject game3;


    public GameObject pauzaText;
    public GameObject pokusajponovno;
    public GameObject meni;
    public GameObject vratiSe;


	// Use this for initialization
	void Start () {
        pauzaText.SetActive(false);
        pokusajponovno.SetActive(false);
        meni.SetActive(false);
        vratiSe.SetActive(false);
	}



    public void VratiSe()
    {
        Time.timeScale = 1;
        game1.SetActive(true);
        game2.SetActive(true);
        game3.SetActive(true);
        pauzaText.SetActive(false);
        pokusajponovno.SetActive(false);
        meni.SetActive(false);
        vratiSe.SetActive(false);
    }

    public void Pau()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    
        Time.timeScale = 0;
        game1.SetActive(false);
        game2.SetActive(false);
        game3.SetActive(false);
        pauzaText.SetActive(true);
        pokusajponovno.SetActive(true);
        meni.SetActive(true);
        vratiSe.SetActive(true);

    }







	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Pau();
        }

        
	}
}
