﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
           if(Time.time > 3)
            {
                SceneManager.LoadScene(1);
            }
        }
		
	}



    public void LoadLevel(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void LoadLevel(int level)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(level);

    }


    public void ExitGame()
    {
        Application.Quit();
    }
}
