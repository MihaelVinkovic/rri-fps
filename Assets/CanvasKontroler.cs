﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CanvasKontroler : MonoBehaviour {

    private Text tekstMisije;
    private Text tekstMunicije;


    public Slider slider;
    public Image fillimage;


    private Color narancasta = new Color(255, 165, 0);
    private Color crvena = Color.red;
    private Color zelena = Color.green;

	// Use this for initialization
	void Start () {
        tekstMisije = GameObject.Find("TextMisije").GetComponent<Text>();
        tekstMunicije = GameObject.Find("TextMunicije").GetComponent<Text>();      


        tekstMisije.text = "Pronađi oružje !";
        tekstMunicije.text = "0";




        slider.value = 0.4f;
    }


    private void Update()
    {
       if(slider.value < 0.7f && slider.value > 0.4f)
       {
            fillimage.color = narancasta;
       }
        else if (slider.value <= 0.4f)
       {
            fillimage.color = crvena;
        }
        else
        {
            fillimage.color = zelena;
        }
    }


    public void PostaviTekstZaMuniciju(string tekst)
    {
        tekstMunicije.text = tekst;
    }

    public void PostaviTekstZaMisiju(string tekst)
    {
        tekstMisije.text = tekst;
    }
    

    public void KoličinaŽivota(float zivot)
    {
        slider.value = zivot / 100;
    }





}
